#!/usr/bin/env python

# Dependencies:
# sudo apt-get install -y python-gobject
# sudo apt-get install -y python-smbus
import subprocess
import subprocess as s
import time
import signal
import dbus
import dbus.service
import dbus.mainloop.glib
import logging
from gi.repository import GObject as gobject

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'

LOG_LEVEL = logging.DEBUG
LOG_FILE = "/dev/stdout"
LOG_FORMAT = "%(asctime)s %(levelname)s %(message)s"

"""send notification to desktop- only tempoprary"""
def SetTemporaryNotyficationOoRpiDesktop(msgHeader,msgToSend):
    s.call(['notify-send', '--hint=int:transient:10', '{0}'.format(msgHeader), '{0}'.format(msgToSend)])


"""script for changing from ms to minutes duration using python"""
def convertMsToMins(timeInMs):
    seconds=(timeInMs/1000)%60
    minutes=(timeInMs/(1000*60))%60
    fullDuration = '{0}:{1}'.format(int(minutes),int(seconds))
    return fullDuration


"""Utility functions from bluezutils.py"""
def getManagedObjects():
   bus = dbus.SystemBus()
   manager = dbus.Interface(bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
   return manager.GetManagedObjects()


def findAdapter():
   objects = getManagedObjects()
   bus = dbus.SystemBus()
   for path, ifaces in objects.items():
       adapter = ifaces.get(ADAPTER_IFACE)
       if adapter is None:
           continue
       obj = bus.get_object(SERVICE_NAME, path)
       return dbus.Interface(obj, ADAPTER_IFACE)
   raise Exception("Bluetooth adapter not found")


class BluePlayer(dbus.service.Object):
   AGENT_PATH = "/blueplayer/agent"
   CAPABILITY = "DisplayOnly"

   bus = None
   adapter = None
   device = None
   deviceAlias = None
   player = None
   transport = None
   connected = None
   state = None
   status = None
   discoverable = None
   track = None
   trackFull =None


   def __init__(self):
       self.bus = dbus.SystemBus()
       dbus.service.Object.__init__(self, dbus.SystemBus(), BluePlayer.AGENT_PATH)
       self.bus.add_signal_receiver(self.playerHandler,
               bus_name="org.bluez",
               dbus_interface="org.freedesktop.DBus.Properties",
               signal_name="PropertiesChanged",
               path_keyword="path")

       self.registerAgent()
       adapter_path = findAdapter().object_path
       self.findPlayer()


   def start(self):
       """Start the oneBlue by running the gobject mainloop()"""
       try:
           mainloop = gobject.MainLoop()
           mainloop.run()
       except:
           self.end()


   def findPlayer(self):
       """Find any current media players and associated device"""
       manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
       objects = manager.GetManagedObjects()

       player_path = None
       transport_path = None
       for path, interfaces in objects.items():
           if PLAYER_IFACE in interfaces:
               player_path = path
           if TRANSPORT_IFACE in interfaces:
               transport_path = path

       if player_path:
           print('Found player on path [{}]'.format(player_path))
           self.connected = True
           self.getPlayer(player_path)
           player_properties = self.player.GetAll(PLAYER_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
           if "Status" in player_properties:
               self.status = player_properties["Status"]
           if "Track" in player_properties:
               self.track = player_properties["Track"]
       else:
           print('Could not find a player')

       if transport_path:
           #logging.debug("Found transport on path [{}]".format(player_path))
           self.transport = self.bus.get_object("org.bluez", transport_path)
           #logging.debug("Transport [{}] has been set".format(transport_path))
           transport_properties = self.transport.GetAll(TRANSPORT_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
           if "State" in transport_properties:
               self.state = transport_properties["State"]


   def getPlayer(self, path):
       """Get a media player from a dbus path, and the associated device"""
       self.player = self.bus.get_object("org.bluez", path)
       print('Player [{}] has been set'.format(path))
       device_path = self.player.Get("org.bluez.MediaPlayer1", "Device", dbus_interface="org.freedesktop.DBus.Properties")
       self.getDevice(device_path)


   def getDevice(self, path):
       """Get a device from a dbus path"""
       self.device = self.bus.get_object("org.bluez", path)
       self.deviceAlias = self.device.Get(DEVICE_IFACE, "Alias", dbus_interface="org.freedesktop.DBus.Properties")


   def playerHandler(self, interface, changed, invalidated, path):
       """Handle relevant property change signals"""
       iface = interface[interface.rfind(".") + 1:]

       if iface == "Device1":
           if "Connected" in changed:
               self.connected = changed["Connected"]
       if iface == "MediaControl1":
           if "Connected" in changed:
               self.connected = changed["Connected"]
               if changed["Connected"]:
                   print('MediaControl is connected [{0}] and interface [{1}]'.format(path,iface))
                   self.findPlayer()
       elif iface == "MediaTransport1":
           if "State" in changed:
               self.state = (changed["State"])
           if "Connected" in changed:
               self.connected = changed["Connected"]
       elif iface == "MediaPlayer1":
           if "Track" in changed:
               self.track = changed["Track"]
               if "Artist" in self.track:
                   print('Artist: {0}'.format(self.track["Artist"]))
                   trackFull = self.track["Artist"]
               if "Title" in self.track:
                   print('Track name: {0}'.format(self.track["Title"]))
                   trackFull=trackFull + self.track["Title"]
               if "Duration" in self.track:
                   #convert ms duration to minutes
                   fullDuration =convertMsToMins(self.track["Duration"])
                   print('Duration: {0}'.format(convertMsToMins(self.track["Duration"])))
               SetTemporaryNotyficationOoRpiDesktop('Playing now...{0}'.format(fullDuration), trackFull)
           if "Status" in changed:
               self.status = (changed["Status"])
       #SetTemporaryNotyficationOoRpiDesktop('Status', changed["Status"])


   """Pairing agent methods"""
   @dbus.service.method(AGENT_IFACE, in_signature="ou", out_signature="")
   def RequestConfirmation(self, device, passkey):
       self.trustDevice(device)
       return

   @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
   def AuthorizeService(self, device, uuid):
       return

   def trustDevice(self, path):
       """Set the device to trusted"""
       device_properties = dbus.Interface(self.bus.get_object(SERVICE_NAME, path), "org.freedesktop.DBus.Properties")
       device_properties.Set(DEVICE_IFACE, "Trusted", True)

   def registerAgent(self):
       """Register BluePlayer as the default agent"""
       manager = dbus.Interface(self.bus.get_object(SERVICE_NAME, "/org/bluez"), "org.bluez.AgentManager1")
       manager.RegisterAgent(BluePlayer.AGENT_PATH, BluePlayer.CAPABILITY)
       manager.RequestDefaultAgent(BluePlayer.AGENT_PATH)

   def startPairing(self):
       """Make the adpater discoverable"""
       adapter_path = findAdapter().object_path
       adapter = dbus.Interface(self.bus.get_object(SERVICE_NAME, adapter_path), "org.freedesktop.DBus.Properties")
       adapter.Set(ADAPTER_IFACE, "Discoverable", True)


gobject.threads_init()
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

player = None
try:
    s.call('/usr/lib/notification-daemon/notification-daemon &', shell=True)
    print('oneBlue')
    player = BluePlayer()
    mainloop = gobject.MainLoop()
    mainloop.run()
except KeyboardInterrupt as ex:
    print('Program end by Endpointuser')
except Exception as ex:
    print('Something went wrong: {0}'.format(ex))